This is a random Javascript:    

        :::javascript
        if (window.name=='' || (window.name.indexOf('wicket') > -1 && window.name!='wicket-wicket:default')) { 
        window.location="register/wicket:pageMapName/wicket-0" + (window.location.hash != null ? window.location.hash : ""); 
        }

This is some very random Java:

    :::java
        function foo() {
            int poo = 1;
                return;
        }

Here is some random Python

    :::python
        friends = ['john', 'pat', 'gary', 'michael']
        for i, name in enumerate(friends):
            print "iteration {iteration} is {name}".format(iteration=i, name=name)

Try your own!